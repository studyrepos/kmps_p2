import scala.io.Source

object Main {

  case class Track(title: String = "", length: String = "", rating: Int = 0, features: List[String] = List(), writers: List[String] = List())

  case class Album(title: String = "", date: String = "", artist: String = "", tracks: List[Track] = List())

  /**
    * Parse XML-File given as characterlist to list of tokens(String)
    * @param content  :   filecontent as characterlist
    * @param tokens   :   list of found tokenstrings
    * @return tokens as list of strings
    */
  def createTokenList(content: List[Char], tokens: List[String] = List()): List[String] = content match {
    case Nil => tokens
    case '\r' :: tail => createTokenList(tail, tokens)
    case '\n' :: tail => createTokenList(tail, tokens)
    case '\t' :: tail => createTokenList(tail, tokens)
    case '<'  :: tail =>
      val token: (List[Char], String) = getToken(tail, '>', "")
      createTokenList(token._1.tail, tokens :+ token._2)
    case _  =>
      val token: (List[Char], String) = getToken(content, '<', "")
      createTokenList(token._1, tokens :+ token._2)
  }

  /**
    * Parse single token from characterlist, delimited by pattern
    * @param content  : characterlist to parse
    * @param pattern  : delimiter patter for token
    * @param token    : actual token
    * @return tupel of remaining characterlist and found token
    */
  def getToken(content: List[Char], pattern: Char, token: String): (List[Char], String) = content match {
    case Nil => (content, token)
    case `pattern` :: _ => (content, token)
    case _ => getToken(content.tail, pattern, token + content.head)
  }

  /**
    * Parse list of tokenstrings to list of albums
    * @param tokenList  : list of tokenstrings
    * @param albums     : actual list of albums
    * @return list of albums
    */
  def parseTokenList(tokenList: List[String], albums: List[Album] = List()): List[Album] = tokenList match {
    case Nil => albums
    case "album" :: tail =>
      val album: (List[String], Album) = createAlbum(tail)
      parseTokenList(album._1, albums :+ album._2)
    case _ :: tail => parseTokenList(tail, albums)
  }

  /**
    * Parse tokenstrings to single album
    * @param tokenList  : list of tokenstrings
    * @param album      : actual album
    * @return tupel of remaining list of tokenstrings and parsed album
    */
  def createAlbum(tokenList: List[String], album: Album = Album()): (List[String], Album) = tokenList match {
    case Nil => (tokenList, album)
    case "/album" :: tail => (tail, album)
    case "title" :: tail => createAlbum(tail.tail.tail, album.copy(title = tail.head))
    case "artist" :: tail => createAlbum(tail.tail.tail, album.copy(artist = tail.head))
    case "date" :: tail => createAlbum(tail.tail.tail, album.copy(date = tail.head))
    case "track" :: tail =>
      val track: (List[String], Track) = createTrack(tail)
      createAlbum(track._1, album.copy(tracks = album.tracks :+ track._2))
    case _ :: tail => createAlbum(tail, album)
  }

  /**
    * Parse tokenstrings to single track
    * @param tokenList  : list of tokenstrings
    * @param track      : actual track
    * @return tupel of remaining list of tokenstrings and parsed track
    */
  def createTrack(tokenList: List[String], track: Track = Track()): (List[String], Track) = tokenList match {
    case Nil => (tokenList, track)
    case "/track" :: tail => (tail, track)
    case "title" :: tail => createTrack(tail.tail.tail, track.copy(title = tail.head))
    case "length" :: tail => createTrack(tail.tail.tail, track.copy(length = tail.head))
    case "rating" :: tail => createTrack(tail.tail.tail, track.copy(rating = tail.head.toInt))
    case "feature" :: tail => createTrack(tail.tail.tail, track.copy(features = track.features :+ tail.head))
    case "writing" :: tail => createTrack(tail.tail.tail, track.copy(writers = track.writers :+ tail.head))
    case _ :: tail => createTrack(tail, track)
  }


  /**
    * Main entry method
    * @param args
    */
  def main(args: Array[String]): Unit = {
    println("Praktikum 2 - Aufgabe 2-3")

    val chars: List[Char] = Source.fromFile("alben.xml").mkString.toCharArray.toList

    val tokenList: List[String] = createTokenList(chars)

    printTokenList(tokenList)

    val alben: List[Album] = parseTokenList(tokenList)
    println(alben)
  }

  def printTokenList(list: List[String]): Boolean = list match {
    case Nil => print('\n'); true
    case _  =>
      print(list.head + ", ")
      printTokenList(list.tail)
  }
}



