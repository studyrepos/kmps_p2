
/**
  * Multiplys result of f(x)[a,b]
  * @param f  : function to evaluate
  * @param a  : lower bound
  * @param b  : upper bound
  * @param p  : product
  * @return product of function results
  */
def prod(f: Int => Int, a: Int, b: Int,  p: Int = 1): Int = a match {
  case `b` => p * f(a)
  case _ => prod(f, a + 1, b, p * f(a))
}
// call prod for id[1,3] : 6
prod(x => x, 1, 3)

// call prod for square[2,4] : 576
prod(x => x * x, 2, 4)


/**
  *
  * @param f  : function to evaluate
  * @param p  : actual product, needed for tail recursion
  * @param a  : lower bound
  * @param b  : upper bound
  * @return   : product of function results f(x)[a,b]
  */
def prodCurry(f: Int => Int, p: Int = 1)(a: Int, b: Int): Int = a match {
  case `b` => p * f(a)
  case _ => prodCurry(f, p * f(a))(a + 1, b)
}

/**
  * Product of positiv integers from a to b
  * @return
  */
def prodInt : (Int, Int) => Int = prodCurry(x => x)

// call : 60
prodInt(3, 5)


/**
  * Evaluates factorial of positiv integer
  * @param a  :
  * @return
  */
def fak(a: Int): Int = prodInt(1,a)

// call : 5040
fak(7)
